 
    // Create a fetch request using the GET method that will retrieve ALL the to do list items from JSON Placeholder API.
    // Using the data retrieved, create an array using the map method to return ONLY the title of every item and print the result in the console.
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => {
    let titles = json.map(item => item.title);
    console.log(titles);
})

    

/*    Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
    Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.*/
    
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(data => {
console.log(`The item "${data.title}" on the list has a status of ${data.completed ? 'completed' : 'incomplete'}`);
});


    // Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos', {
        method: 'POST',
        body: JSON.stringify({
            title: 'Created a To Do list item',
            completed: false,
            userId: 1
        }),
        headers: {'Content-type': 'application/json'},
    })
.then(response => response.json())
.then(json => console.log(json))
    
    // Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
    // Update a to do list item by changing the data structure to contain the following properties:
    // Title
    // Description
    // Status
    // Date Completed
    // User ID
fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: 1,
            title: 'Update To Do List Item',
            description: 'This is an updated task',
            completed: true,
            dateCompleted: 'Pending',
            userId: 1
        }),
        
        headers: {'Content-type': 'application/json'},
})
.then(response => response.json())
.then(json => console.log(json))
    
    // Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
    // Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PATCH',
        body: JSON.stringify({
            completed: true,
            dateCompleted: '2023-03-29',
            id: 1,

        }),
        headers: {'Content-type': 'application/json'},
})
.then(response => response.json())
.then(json => console.log(json))

    // Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
method: 'DELETE'
})

  /*  Create a request via Postman to retrieve all the to do list items.
    GET HTTP method
    https://jsonplaceholder.typicode.com/todos URI endpoint
    Save this request as GET all to do list items
    
    Create a request via Postman to retrieve an individual to do list item.
    GET HTTP method
    https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    Save this request as GET to do list item
    .
    Create a request via Postman to create a to do list item.
    POST HTTP method
    https://jsonplaceholder.typicode.com/todos URI endpoint
    Save this request as CREATE to do list item

    Create a request via Postman to update to do list item using PUT.
    PUT HTTP method
    https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    Save this request as UPDATE to do list item

    Update the to do list item to mirror the data structure used in the PUT fetch request
    Create a request via Postman to update a to do list item using PATCH.
    PATCH HTTP method
    https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    Save this request as PATCH to do list item
    Update the to do list item to mirror the data structure of the PATCH fetch request
    
    Create a request via Postman to delete a to do list item.
    DELETE HTTP method
    https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    Save this request as DELETE to do list item

    